<?php

class LightningClient 
{
    
    private function LnRest($add,$post= "")
    {
        $url = 'https://localhost:8080'.$add;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(!empty($post))
        {
            curl_setopt($ch,CURLOPT_POST, 1);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $post);
        }
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    private function LnRestDEL($add)
    {
        $url = 'https://localhost:8080'.$add;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        
        $data = curl_exec($ch);
        curl_close($ch);
        
        return $data;
    }
    public function closechannel($tx_id,$force)
    {
        $tx_id = explode(':',$tx_id);
        ($force)? $force = '?force=true': "";
        $hex_id =$tx_id['0'].'/'.$tx_id['1'].$force;

        $url = '/v1/channels/'.$hex_id;
        echo $url ;
       
        $res = LightningClient::LnRestDEL($url);
        return json_decode($res);        
        
    }
    public function testconnect(){
        $res = LightningClient::LnRest('/v1/getinfo');
        if(empty($res)):
            return false;
        else:
            return true;
        endif;
    }

    public function getInfo() {
        $res = LightningClient::LnRest('/v1/getinfo');
        return json_decode($res,true);
        
    }
    public function getNodeInfos($pub_key) {
        $res = LightningClient::LnRest('/v1/graph/node/'.$pub_key);
        return json_decode($res,true);
        
    }
    public function testunlock () {
        $info = json_decode(LightningClient::LnRest('/v1/getinfo'),true);
        if(!empty($info)):        
            return true; 
        else:
            return false;
        endif;
    }
     public function unlockwallet($pass) {
        $pass =  json_encode(array('password' => base64_encode($pass)));
        $res = LightningClient::LnRest('/v1/unlockwallet',$pass);
        return json_decode($res,true);
    }
    public function create_wallet($pass) {
        $pass =  json_encode(array('password' => base64_encode($pass)));
        $res = LightningClient::LnRest('/v1/createwallet',$pass);
        return json_decode($res,true);
    }
    public function getWalletBalance() {
        $res = LightningClient::LnRest('/v1/balance/blockchain');
        return json_decode($res,true);
    }

    public function getChannelBalance() {
        $res = LightningClient::LnRest('/v1/balance/channels');
        return json_decode($res,true);
    }
    public function getAllchannels() {
        $res = LightningClient::LnRest('/v1/channels');
        return json_decode($res,true);
    }

    public function getPendingChannels() {
        $res = LightningClient::LnRest('/v1/channels/pending');
        return json_decode($res,true);
    }
    
    public function getTransactions() {
        $res = LightningClient::LnRest('/v1/transactions');
        return json_decode($res,true);
    }

    public function getPeers() {
     
        $res = LightningClient::LnRest('/v1/peers');
        return json_decode($res,true);
    }
    public function connectPeers($addr,$perm = false) { 
        $addr = explode("@",$addr);
        $host = $addr[1];
        $node = $addr[0];


        $peers = json_encode(array(
                    'addr' => array ("host"=> $host,"pubkey"=>$node),
                    'perm'=> $perm
                ));
         $res = LightningClient::LnRest('/v1/peers',$peers);
        return json_decode($res,true);
    }
     public function getPendigChannels() {
     
        $res = LightningClient::LnRest('/v1/channels/pending');
        return json_decode($res,true);
    }
    public function getInvoices($pending_only = false) {
          $res = LightningClient::LnRest('/v1/invoices');
        return json_decode($res,true);
    }

    
    public function getPayments() {
      
        $res = LightningClient::LnRest('/v1/payments');
        return json_decode($res,true);

    }
    public function payinvoice($invoice) {
      
        $res = LightningClient::LnRest('/v1/payreq/' . $invoice);
        return json_decode($res,true);

    }
    public function openchannel($nodePubkeyString,$localFundingAmount,$pushSat,$targetConf = "",$satPerByte =""){
     
        $array = json_encode(array(
                    'node_pubkey_string' =>  $nodePubkeyString,
                    'local_funding_amount' => $localFundingAmount,
                    'push_sat' => $pushSat
                
                ));


        $res = LightningClient::LnRest('/v1/channels',$array);
         return json_decode($res,true);
    }

    public function sendpayment($destination) {
      
        $payreq = array('payment_request' => $destination);
        $payreq = json_encode($payreq);
        $res = LightningClient::LnRest('/v1/channels/transactions',$payreq);
        return json_decode($res,true);

    }
    public function sendonchain($addr,$amt) {
      $send = json_encode(array("addr" => $addr,
            "amount" => $amt,
            "target_conf"=> 0,
            
        ));
        
        $res = LightningClient::LnRest('/v1/transactions',$send);
        return json_decode($res,true);

    }

    public function addinvoice($value,$memo ="") {
        $value = array('value'=>$value);
        if(!empty($memo)): $value['memo'] = $memo; endif;
        $value = json_encode($value);         
        $res = LightningClient::LnRest('/v1/invoices',$value);
        return json_decode($res,true);

    }
    public function newadd($type = "") {

        $res =LightningClient::LnRest('/v1/newaddress');
        return json_decode($res,true);
    }

}
?>
