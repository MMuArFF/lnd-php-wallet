<?php 
include('conf.php');
$client = New LightningClient();
function in_array_r($needle, $haystack, $strict = true) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
function generateqr($url){
    return '<img src="http://chart.apis.google.com/chart?cht=qr&chs=250x250&chl='.$url.'" class="img-responsive center-block">';
}
function lookupinvoice($ln_invoice){
	global $client;
        
        $check = $client->getInvoices();
        $invoices = array_column($check['invoices'], 'payment_request');
		$invoice_key = array_search($ln_invoice, $invoices);
		if(isset($check['invoices'][$invoice_key]['settled']))
        {
            return true;
        } 
        else { return false;}
}
function template(string $file, array $variables = []): string
{
    extract($variables, EXTR_SKIP);
    ob_start();
    include('../view/lang.php');
    include($file);
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
}
function balance($transactions)
{
    $sum = array("out"=> 0, "in" => 0,"total"=>0);
    foreach ($transactions as $i => $rrow)
    { 
        foreach ($rrow as $ii => $row) 
        {
            $sum['total'] += $row['amount'];
            if($row['amount'] < 0)
            {
                $sum['out'] += abs($row['amount']);
            }
            else
            {
                $sum['in'] += $row['amount'];
            }

        }
    }
    return $sum;
}

function print_array($array) {

    print("<pre>".print_r($array,true)."</pre>");
}
function paginate_array($array,$nbperpage) {
    $nb_tot = count($array);
    $nbpage = $nb_tot / $nbperpage;
    for ($i=0; $i < ceil($nbpage) ; $i++) { 
        $ii = $i * $nbperpage;
        $iii = -($nb_tot - $nbperpage) + $ii;
        $pages = array_slice($array, $ii, $iii, true);
        if(!empty($pages)){
        $page[$i] = $pages;
        }
    }
    if(empty($page)){$page[0]= "";}
    return $page;
}
function getActiveandInactiveChannels($chans)
{
    $active =array();
    $inactive = array();
    foreach ($chans as $key => $e) {
        foreach ($e as $key2 => $channel) {
            isset($channel['active']) ? array_push($active,$key2) : array_push($inactive,$key2);
        }
    }
    $channels = array($active,$inactive);
    return $channels;
}
function logs_on()
{
    global $conf;
    if($fp = fsockopen('127.0.0.1',$conf['logs_port'],$errCode,$errStr,5) && $conf['logs']==true){   
       return true;
    } else {
       return false;
    } 
    fclose($fp);
}
