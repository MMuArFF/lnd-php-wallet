<?php
session_start();
ini_set('display_errors', '0');
include("../view/lang.php");
include('../controler/api.php');
include('../controler/func.php');

$client = new LightningClient();
if(isset($_GET['EN']) OR isset($_GET['FR'])): $langChanged = true; else: $langChanged = false; endif;

$page = key($_GET) ?? 'receive';
$page_secure = array('send','balance','channels','decode','list','listout','nodeinfo','transactions','peers','receive','sign','verify','logs');

if(in_array($page, $page_secure,true)){
  
    include('../models/'.$page.'.php');
    include('../models/menu.php');
    include('../models/footer.php');
    include('../models/header.php');
    $menu = menuAction($page);
    $header = headerAction($page);
    $footer = footerAction();
    $array  = array(
        'menu' => $menu,
        'langChanged' => $langChanged,
        'header' => $header,
        'footer' => $footer,
        'lnd_on' => true,
        'connected' => true);
    if(isset($_POST['pwd'])):
        $array['connect_response'] = $client->unlockwallet($_POST['pwd']);
        
        if(isset($array['connect_response']['error']) && ($array['connect_response']['error']== 'wallet not found')):
            $array['connect_response'] = $client->create_wallet($_POST['pwd']);
        endif;
    endif;
    $con = $client->testconnect();

    if(!$con):
        $array['content'] = "";
        $array['connected'] = false;
        $array['lnd_on'] = false;

    
    else:
        $info = $client->getInfo();
        //var_dump($info);
        if(!isset($info['synced_to_chain'])):
            $array['unlock'] = $client->testunlock();
            $array['lnd_on'] = true;
            $array['content'] = "";
            $array['connected'] = false;
        else:
            $content = Action($client);
            $array['content'] = $content;
        endif;
    endif;

    echo template("../view/base.html.php", $array);


} 
else {
     header("Status", true, 403);
} 
