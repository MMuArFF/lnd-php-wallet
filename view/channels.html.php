<?php 
if(isset($_POST['chan_node_addr'])&& isset($_POST['loc_amt']) && isset($_POST['rmt_amt']))
{
  if(!empty($openchannel['funding_txid_bytes']))
  {
      echo '<div class="alert-info alert">Transaction : '.bin2hex($openchannel['funding_txid_bytes']).'</div>';
  }
  else
  { 
     echo '<div class="alert alert-danger">'.print_array($openchannel).'</div>';  
  }
 
}
if(isset($_POST['tx_id'])):
  $tx_id = explode(':',$_POST['tx_id']);
  print_array($closechannel);
  if(!empty($closechannel['closing_txid'])):
    ?>
    <div class="alert-info alert">Transaction : <?= $closechannel['closing_txid']?></div>
  <?php else: ?>
    <div class="alert alert-danger">NOT WORKING FOR NOW <br /> consider using : lncli --no-macaroons --tlscertpath=/home/lnd/.lnd/tls.cert closechannel <?= $tx_id[0] ?> --force <br /><?= $closechannel['error']?></div>
  <?php endif;
endif;

if(empty($chans)) { goto pending_channels;}
$chans = $chans['channels'];
//print_array($channels);
foreach ($channels as $key => $value) 
{ 

($key == 0) ? $h2title = $_lang_activated_chans : $h2title = $_lang_inactivated_chans
?>
<h2><?= $h2title ?></h2>
   <table class="table table-responsive">
   	<thead>
   		<th><?= $_lang_node_hash ?></th>
      <th>Alias</th>
   		<th><?= $_lang_tx_char ?></th>
   		<th><?= $_lang_capacity ?></th>
   		<th><?= $_lang_local_amount ?></th>
   		<th><?= $_lang_remote_amount ?></th>
      <th>Infos</th>
   	</thead>
   	<tbody>
   		<?php

	foreach ($value as $key => $i) {
    	
		?>
		<tr> 
  			<td width="300px" ><div style="width: 300px;  overflow: auto"><?=$chans[$i]['remote_pubkey'] ?></div>
  </td>


      <td><font color="<?= $nodes_info_chans[$i]['node']['color']?>"><?= $nodes_info_chans[$i]['node']['alias']?></td>
          

      		<td><a href="https://live.blockcypher.com/btc-testnet/tx/<?=substr($chans[$i]['channel_point'],0,-2) ?>"><?=substr($chans[$i]['channel_point'],0,20) ?>...</a></td>
          
      		<td><?=$chans[$i]['capacity'] ?></td>
          <td><?=(isset($chans[$i]['local_balance']) ? $chans[$i]['local_balance'] : "0")?></td>
     		<td><?=(isset($chans[$i]['remote_balance']) ? $chans[$i]['remote_balance'] : "0") ?></td>
        <td><a class="button" href="?channels&id=<?= $i?>#popup1">?</a></form></td>
          
 		</tr><?php 
	}?>
	</tbody></table>
<?php
}
if(isset($_GET['id'])): 
  isset($node_info['node']['addresses'])? $address = '@'.$node_info['node']['addresses'][0]['addr'] : $address= ''; ?>
    <div id="popup1" class="overlay">
  <div class="popup">
    <?php $idchan = $_GET['id'] ?>
    
      <form method="POST" action="?channels"><?= isset($chans[$idchan]['active'])? '<input type="hidden" name="active" value="">' : '' ?><input type="hidden" name="tx_id" value="<?= $chans[$idchan]['channel_point']?>"><button class="btn btn-danger">Close Channel <i class="fa fa-window-close" aria-hidden="true"></i></button></form>
    <h2> Node Info</h2>
    <a class="close" href="#">&times;</a>
    <div class="content"><?php $idchan = $_GET['id'] ?>
     <?= $chans[$idchan]['remote_pubkey'].$address  ?><br />
     Alias : <?=  $node_info['node']['alias'] ?>
     Channels : <?= $node_info['num_channels'] ?>
     <?=  generateqr($chans[$idchan]['remote_pubkey'].$address) ?>

    </div>
    <h2> Chan Info</h2>
    <?=print_array($chans[$idchan])?>
    <div class="content">


    </div>
  </div>
</div>
<?php 
endif;



pending_channels: 
if(empty($pending_channels)): goto add_chan; endif; ?>
<h2><?= $_lang_pending ?></h2>
   <table class="table table-responsive">
    <thead>
      <th>Status</th>
      <th><?= $_lang_node_hash ?></th>
      <th>Alias</th>
      <th>Transaction</th>
      <th><?= $_lang_capacity ?></th>
      <th><?= $_lang_local_amount ?></th>
      <th><?= $_lang_remote_amount ?></th>
    </thead>
    <tbody>

 <?php
 $iii = 0;
  
  if(!empty($pending_channels['pending_open_channels'])):
  
  foreach ($pending_channels['pending_open_channels'] as $keyy => $ii):
  ?>
  <tr class="alert-success">
        <td><?= $_lang_opening ?></td> 
        <td width="250px" ><div style="width: 250px;  overflow: auto"><?=$ii['channel']['remote_node_pub']?></div></td>
        <td><font color="<?= $nodes_info_chans_pending[$iii]['node']['color']?>"><?= $nodes_info_chans_pending[$iii]['node']['alias']?></td>
        <td><a href="https://live.blockcypher.com/btc-testnet/tx/<?= substr($ii['channel']['channel_point'],0,-2)?>"><?=substr($ii['channel']['channel_point'],0,20) ?>...</a></td>
        <td><?=$ii['channel']['capacity']?></td>
        <td><?=(isset($ii['channel']['local_balance']) ? $ii['channel']['local_balance'] : "0")?></td>
        <td><?=(isset($ii['channel']['remote_balance']) ? $ii['channel']['remote_balance'] : "0") ?></td>
      </tr>
   <?php
   $iii++;
  endforeach;
  endif;
  if(!empty($pending_channels['pending_closing_channels'])):
    
      foreach ($pending_channels['pending_closing_channels']as $keyy => $ii): 
        ?>
      <tr class="alert-info"> 
        <td><?= $_lang_close ?></td>
       <td width="250px" ><div style="width: 250px;  overflow: auto"><?=$ii['channel']['remote_node_pub']?></div></td>
        <td><?=$nodes_info_chans_pending[$iii]['node']['alias']?></td>
        <td><a href="https://live.blockcypher.com/btc-testnet/tx/<?=substr($ii['channel']['channel_point'],0,-2)?>"><?=substr($ii['channel']['channel_point'],0,20) ?>...</a></td>
        <td><?=$ii['channel']['capacity']?></td>
        <td><?=(isset($ii['channel']['local_balance']) ? $ii['channel']['local_balance'] : "0")?></td>
        <td><?=(isset($ii['channel']['remote_balance']) ? $ii['channel']['remote_balance'] : "0") ?></td>
      </tr>
<?php $iii++;
      endforeach;
    
  endif; 
  if(!empty($pending_channels['pending_force_closing_channels'])):
   
      foreach ($pending_channels['pending_force_closing_channels'] as $keyy => $ii): 
        ;?>
      <tr class="alert-danger"> 
        <td><?= $_lang_force_close ?></td>
        <td width="250px" ><div style="width: 250px;  overflow: auto"><?=$ii['channel']['remote_node_pub']?></div></td>
        <td><?=$nodes_info_chans_pending[$iii]['node']['alias']?></td>
        <td><a href="https://live.blockcypher.com/btc-testnet/tx/<?=substr($ii['channel']['channel_point'],0,-2)?>"><?=substr($ii['channel']['channel_point'],0,20) ?>...</a></td>
        <td><?=$ii['channel']['capacity']?></td>
        <td><?=(isset($ii['channel']['local_balance']) ? $ii['channel']['local_balance'] : "0")?></td>
        <td><?=(isset($ii['channel']['remote_balance']) ? $ii['channel']['remote_balance'] : "0") ?></td>
      </tr>
<?php
      $iii++;
      endforeach;
    
  endif;?>

        
</tbody></table>

<?php add_chan: ?>

<h2><?= $_lang_ajout_chan ?></h2>
    <form class="form-horizontal" method="post">
        <fieldset style="position: sticky;">
    	    <div class="input-group col-xs-8" >
      			<span class="input-group-addon"><?=$_lang_local_amount ?></span>
      			<input id="prependedtext" name="loc_amt" class="form-control" placeholder="100000" required="" type="number">
    			<span class="input-group-addon"><?=$_lang_remote_amount ?></span>
      			<input id="prependedtext2" name="rmt_amt" class="form-control" placeholder="50000" required="" type="number">
    		</div>
            <div class="input-group col-md-8">
                <span class="input-group-addon">Node key</span>
              	<select type="select" name="chan_node_addr" class="form-control" aria-label="node key">
              		<?php
              		if(!empty($peers))
                    {
                    	  
                       	$peers = $peers['peers'];
                      	foreach ($peers as $key => $value) {
                        	if(!in_array_r($value['pub_key'],$chans)){
                            $chan_connect ="";
                        	?>
                        		<option value="<?=$value['pub_key']?>"><?=$value['pub_key']?></option>
                    		<?php
                    		  }
                        }
                        if(!isset($chan_connect)): ?><option value="" selected disabled><?= $_lang_ajout_peer_before ?></option><?php 
                        endif;
                    }
                    else 
                    { 
                        ?>
                        	<option value="" selected disabled><?= $_lang_ajout_peer_before ?></option>
                    	<?php
                    }?>

                      </select>
              	</select>
              	<span class="input-group-btn"><input type="submit" class="btn btn-secondary"  ">Go!</input></span>   
            </div>
        </fieldset>
    </form>
    