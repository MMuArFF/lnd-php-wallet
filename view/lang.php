<?php

if(!isset($_SESSION['lang'])) { $_SESSION['lang']='EN'; }
if(isset($_GET['FR'])){ $_SESSION['lang'] = "FR";}
if(isset($_GET['EN'])){ $_SESSION['lang'] = "EN";}
$lang = $_SESSION['lang'];

if($lang == "FR")
{
    $_lang_closing = "Fermeture";
    $_lang_update = 'Vu le';
    $_lang_decode = "Décode";
    $_lang_sign = "Signe";
    $_lang_verify = "Verifie";
    $_lang_source = 'Voir code source';
    $_lang_intro = 'Portefeuille Lightning';
    $_lang_invalid = 'Addresse du noeud invalide';
    $_lang_lnd_off="Noeud non lancé";
    $_lang_unlock = "Déverouiller ou creez"; 
    $_lang_not_connected ="noeud non Déverouillé ou non Synchronisé";
    $_lang_close = "fermer";
    $_lang_changed = "Langue changée";
    $_lang_send_onchain = "Envoyez des tBTC";
	$_lang_facture = "Crééz une facture";
    $_lang_amt = "Montant";
;	$_lang_send = "Facture : ";
	$_lang_payed = "J'ai payé";
	$_lang_payed_true = "Merci d'avoir payé, LND rocks !";
	$_lang_payed_false = "Nous n'avons pas reçu le paiement !";
	$_lang_send_wallet = "Envoyer";
	$_lang_receive_wallet = "Recevoir";
	$_lang_paiement_list_in = "Liste de facture reçues";
    $_lang_paiement_list_out = "Liste de paiement envoyés";
	$_lang_list_in = "Reçues";
    $_lang_list_out = "Envoyés";
	$_lang_paiement_titre = "Payez une facture";
	$_lang_ln_invoice = "Facture lightning";
	$_lang_balance = "Balance";
	$_lang_node_info = "Noeud";
	$_lang_channels = "Tunnels";
	$_lang_wallet_balance = "Portefeuille personnel";
	$_lang_channel_balance = "Portefeuille Tunnels";
	$_lang_synchro = "Synchronisé au reseau";
    $_lang_block_height = "Nombre de block";
    $_lang_block_hash = "Dernier Hash";
    $_lang_node_network = "Etat réseau";
    $_lang_node_node = "Etat du node";
    $_lang_network = "Réseau";
    $_lang_ajout_peer = "Ajouter un pair";
    $_lang_ajout_peer_before = "Ajouter un pair avant de vous creer un channel";
    $_lang_peer_added = "Pair ajouté !";
    $_lang_ajout_chan = "Ajouter un Tunnel";
    $_lang_yes = "Oui";
    $_lang_no = "Non";
    $_lang_node_hash = "Identité du noeud";
    $_lang_nb_peers = "Nombre de pairs connectés";
    $_lang_nb_chans ="Nombre de Tunnel connectés";
    $_lang_remote_amount = "Montant distant";
    $_lang_local_amount = "Montant local";
    $_lang_payment_send = "Paiement envoyé";
    $_lang_addr = "addresse";
    $_lang_new_addr_create = "Nouvelle addresse crée : ";
    $_lang_new_addr = "Créez une nouvelle addresse";
    $_lang_tx_char = "Transaction de chargement";
    $_lang_pay = "payé";
    $_lang_connected = "Connecté";
    $_lang_connected_peers  = "Pairs connectés";
    $_lang_ln_payment = "Hash paiement";
    $_lang_hop = "Relais";
    $_lang_paiement_list = "Liste de transactions";
    $_lang_activated_chans = "Tunnels activés";
    $_lang_inactivated_chans = "Tunnels inactifs";
    $_lang_capacity = "Capacité";
    $_lang_peers = "Pairs";
    $_lang_pending = "Channels en attente";
    $_lang_blocks_remaining = "blocks restant";
    $_lang_force_close = "Fermeture forcée";
    $_lang_opening ="Ouverture";
    $_lang_decode_titre = "Decodez une facture";
    $_lang_time_remain = "Temps restant";
    $_lang_time_exceed = "Temps dépassé";
    
}
elseif($lang == "EN")
{
    $_lang_update = 'last update';  
    $_lang_decode = "Decode";
    $_lang_sign = "Sign";
    $_lang_verify = "Verify";
     $_lang_time_remain = "Time remmaining";
    $_lang_time_exceed = "time exceed";
    $_lang_opening = "Opening";
    $_lang_force_close = "Force closing";
    $_lang_source = 'view source code';
    $_lang_intro = 'Lightning Wallet';
    $_lang_blocks_remaining =" blocks remaining";
    $_lang_pending = "Pending Channels";
    $_lang_invalid = 'Node uri invalid';
    $_lang_lnd_off="Node not started";
    $_lang_unlock = "Unlock/Create";
    $_lang_close = "close";
    $_lang_changed = "Language modified";
    $_lang_send_onchain = "Send tBTC";
	$_lang_facture = "Create new invoice";
	$_lang_amt = "Amount";
	$_lang_send = "Invoice : ";
	$_lang_payed = "I have paid ";
	$_lang_payed_true = "Thanks for paying, LND rocks !";
	$_lang_payed_false = "We didn't get the paiement yet";
	$_lang_send_wallet = "Send";
	$_lang_receive_wallet = "Receive";
	$_lang_paiement_list_in = "Invoice list";
    $_lang_paiement_list_out = "Payment list";
	$_lang_transaction_list = "Transaction onChain list";
    $_lang_decode_titre = "Decode Invoice";
	$_lang_list_in = "Received";
    $_lang_list_out = "sent";
	$_lang_paiement_titre = "Pay an invoice";
	$_lang_ln_invoice = "Lightning Invoice";
	$_lang_balance = "Balance";
	$_lang_node_info = "Node";
	$_lang_channels = "channels";
	$_lang_wallet_balance = "Personnal wallet";
	$_lang_channel_balance = "Channel wallet";
	$_lang_synchro = "Synchronised to the network";
    $_lang_block_height = "block number";
    $_lang_block_hash = "Last Hash";
    $_lang_node_network = "Network state";
    $_lang_node_node = "Node state";
    $_lang_network = "Network";
    $_lang_ajout_peer = "Add peer";
    $_lang_ajout_peer_before = "Add a peer before create a channel";
    $_lang_peer_added = "Peer added !";
    $_lang_ajout_chan = "Add  Channel";
    $_lang_yes = "Yes";
    $_lang_no = "No";
    $_lang_node_hash = "Node Identity";
    $_lang_nb_peers = "Number of connected peers";
    $_lang_nb_chans ="Number of connected chans";
    $_lang_remote_amount = "Remote amount";
    $_lang_local_amount = "Local amount";
    $_lang_payment_send = "Payment sent";
    $_lang_addr = "Address";
    $_lang_new_addr_create = "New address : ";
    $_lang_new_addr = "Create a new address";
    $_lang_tx_char = "Funding transaction ";
    $_lang_pay = "Paid";
    $_lang_connected = "Connected";
    $_lang_connected_peers  = "Connected peers";
    $_lang_ln_payment = "payment hash";
    $_lang_hop = "Hop";
    $_lang_paiement_list = "transactions list";
    $_lang_activated_chans = "Activated channels";
    $_lang_inactivated_chans = "Inactivated channels";
    $_lang_capacity = "Capacity";
    $_lang_peers = "Peers";
    $_lang_not_connected ="Node not unlocked or Synchronised";

    $_lang_closing = "Closing";

}
?>
