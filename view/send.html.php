<?php
if(isset($_POST['ln_invoice'])):
    
    if(!empty($res['payment_preimage'])):
       
       
    ?>
        <div class="alert-success alert"><?= $_lang_payment_send ?> <?= $invoice['num_satoshis'] ?> SAT <?= isset($invoice['description']) ?? '' ?> </div><br />
        <div class="alert-info alert">
            Preimage : <?= $res['payment_preimage'] ?><br/>
            Hops : <?= count($res['payment_route']['hops']) ?><br/>
            <?php if(isset($res['payment_route']['total_fees'])): ?> fees : <?= $res['payment_route']['total_fees'] ?> SAT  <br/><?php endif;?>
            <table class="table table-responsive"><thead><th>Chan ID</th><th><?= $_lang_capacity ?></th><th>fee</th></thead><?php foreach ( $res['payment_route']['hops'] as $key => $chan) {
                ?>
                <tr>
                    <td><?= $chan['chan_id'] ?></td>
                    <td><?= $chan['chan_capacity'] ?></td>
                    <td><?= isset($chan['fee']) ?? '0' ?></td>
                </tr>
          <?php  }  ?>
        </table>
        </div>


            
    <?php
    else:
    ?>
    <div class="alert alert-danger"><?= isset($res['payment_error']) ? $res['payment_error']:$res['error']?></div>
    <?php
    endif;
else:

    ?>



<h2><?=$_lang_paiement_titre?></h2>
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <div class="form-group">
                            
                                    <span class="input-group-addon" ><label for="ln_invoice"><?=$_lang_ln_invoice?> :</label> 
                                    <input id="ln_invoice" name="ln_invoice" type="text" required="" placeholder = "ln...."  class="form-control input-md" /></span></div><div class="form-group">
                                    <span class="input-group-addon"><button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i></button></span>
                                </div>
                            
                        
                    </fieldset>
                </form>
            
<?php endif; include('listout.html.php'); ?>
</div><?php

?>