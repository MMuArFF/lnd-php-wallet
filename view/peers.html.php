<?php
if(empty($peers)) { goto newpeer;}
?>
<div class="col-md-10 center-block">
    <h2><?=$_lang_connected_peers ?></h2>
        <table class="table table-responsive">
        	<thead>
        		<th>id</th>
        		<th><?= $_lang_node_hash ?></th>
            <th>Alias</th>
        		<th><?= $_lang_addr ?></th>
        	</thead>
        	<tbody>
        	<?php
        	foreach ($peers as $i => $rrow):
        		foreach ($rrow as $i => $row):

        		?> 
        		<tr>
            		<td><?= $row['peer_id'] ?></td>
                <td><font color="<?=$row['color']?>"><?= $row['pub_key'] ?></td>
                <td><font color="<?=$row['color']?>"><?= $row['node_alias'] ?></font></td>
            		<td><font color="<?=$row['color']?>"><?= $row['address'] ?></td>
          		</tr>
        		<?php
        		endforeach;
        	endforeach;
       		?>
       		</tbody>
       	</table>
<?php
newpeer:
if(isset($_POST['node_addr']))
{
    if(!empty($add_peer)):
      if($add_peer['error'] == "invalid"): ?>
        <div class="alert alert-danger"><?= $_lang_invalid ?></div>
      <?php else: ?>
        <div class="alert alert-danger"><?= $add_peer['error'] ?></div>
      <?php endif; 
    else:?>
    <div class="alert alert-success"><?= $_lang_peer_added ?></div>
<?php
    endif;
}
?>
<h2><?= $_lang_ajout_peer ?></h2>
                <form class="form-horizontal" method="post">
                    <fieldset>
 
                       <div class="input-group">
                      <input type="text" name="node_addr" class="form-control" placeholder="node_key@ip:port" aria-label="node key.">
                      <span class="input-group-btn">
                        <input type="submit"class="btn btn-secondary" type="button" class="btn btn-success">Go!</input>
                      </span>
                    </div>
                  </div>
                    </fieldset>
                </form>
            </div>