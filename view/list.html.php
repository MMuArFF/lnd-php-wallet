<?php
($_GET['page']) ?? $_GET['page'] = 0 ;
?>
<h2><?=$_lang_paiement_list_in ?></h2>
    <table class="table table-responsive">
      	<thead>
      		
      		<th><?=$_lang_ln_payment?></th>
      		<th><?=$_lang_amt?></th>
          <th>memo</th>
      		<th><?=$_lang_pay ?></th>
          <th>details</th>
      		<th>date</th>
      	</thead>
      	<tbody>
          <?php 
          if(count($page)>0){
                 foreach ($list['invoices'] as $i => $listin)
        {
                ?>
              <tr>
                <td width="300" ><div style="width: 300px;  overflow: auto"><?= $listin['payment_request'] ?></div></td>
                <td><?=$listin['value'] ?></td>
                <td><?=(isset($listin['memo'])? $listin['memo']:$_lang_no)?></td>
                <td><?= (isset($listin['settled']) ? $_lang_yes : $_lang_no) ?></td>
                <td><form method="POST" action="?receive#popup1"><input type="hidden" name ="payment" value="<?= base64_encode(serialize($listin)) ?>"><button class="btn btn-info"><i class="fa fa-qrcode" aria-hidden="true"></i></button></form></td>

                <td><?=date('d/m/Y H:i', $listin['creation_date']) ?></td>

              </tr>
              <?php 
            
          }
        }
          ?>
  			
  		</tbody>
  	</table> 
  <div class="text-center">
     <ul class="pagination" ">
       <?php for ($i=0; $i < count($page); $i++) { 
          if($i == $_GET['page']):?><li class = "active"><?php else: ?><li> <?php endif;?>      

         <a href="?receive&page=<?=$i?>"  style="position: initial;"><?=$i ?></a></li>
      <?php } ?>
        
     </ul> 
    </div>
       <div id="popup1" class="overlay">
  <div class="popup">
    <h2> Paiment info</h2>
    <a class="close" href="#">&times;</a>
    <div class="content"><pre><?= print_array(unserialize(base64_decode( $_POST['payment']))) ?></pre>
      
      <?= $qr ?>
    </div>
   
  </div>
</div>