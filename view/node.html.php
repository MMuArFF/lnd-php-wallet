<div class="col-md-10 center-block"> <div  style=" float: left; width:60%">
  <h2><?= $_lang_node_network?></h2>
    <?= $_lang_synchro ?> : <?= isset($info['synced_to_chain']) ? $_lang_yes : $_lang_no ?><br />
    <?= $_lang_block_height?> : <?= $info['block_height']?><br />
    <?=$_lang_block_hash?> : <?= $info['block_hash']?><br />
    <?=$_lang_network?> : <?= isset($info['testnet']) ? "testnet" : "mainnet"?></div> <div  style=" float: left; width:40%">
  <h2><?= $_lang_node_node?></h2>
  <div style="float: left;width:20%"><?= $_lang_node_hash ?> :  </div> <div style="width:70%;overflow: auto;float: left;"><pre > <?= $info['uris'][0]?></pre></div><div style="width:10%;float: left;"> <button class="btn" data-clipboard-action="copy" data-clipboard-text="<?= $info['uris'][0]?>">Copy</button></div>
   <script>
    var clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script><div style="float: left;">
     <?= isset($info['alias'])? "Alias : ".$info['alias'] : "" ?><br/>
    <?= $_lang_nb_peers?> : <?= isset($info['num_peers']) ? $info['num_peers']:'0'?><br />
    <?= $_lang_nb_chans?> : <?= isset($info['num_active_channels']) ? $info['num_active_channels']:'0'?><br /></div>
  </div>  

<div class="col-md-10 center-block" style="margin-top: 20px;"> <?= $qr ?></div>
