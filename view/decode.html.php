<?php

if(isset($_POST['ln_invoice'])):
    if(!empty($invoice)):
       ($invoice['diff_time']< 0) ? $class = "alert-success alert" : $class="alert-danger alert";
    ?>
        <div class="<?=$class?>">
        <?= $_lang_amt ?> : <?=$invoice['num_satoshis'] ?> SAT    <?= isset($invoice['description']) ? '|| description : '.$invoice['description']: ''; ?> ||
        <?= ($invoice['diff_time']< 0 ) ? $_lang_time_remain .' : '.abs($invoice['diff_time']).' sec' : $_lang_time_exceed.' : '.$invoice['diff_time'].'sec' ?>
        
       
    </div>
        <h4>Node destination info :</h4>
        <table class="table table-responsive">
        <thead><thead><th>Node</th><th><?= $_lang_capacity ?></th><th><?= $_lang_channels ?></th><th>Alias</th><th><?= $_lang_update ?></th></thead>
        <tbody>
         <tr bgcolor="#<?= $node_info['node']['color'] ?>">
            <td><?= $node_info['node']['pub_key'] ?></td>
            <td><?= $node_info['total_capacity'] ?></td>
            <td><?= $node_info['num_channels'] ?></td>
            <td><?= $node_info['node']['alias'] ?></td>
            <td><?= date("m/d/Y H:i", $node_info['node']['last_update']) ?></td>
        </tr>
        </tbody>
    </table>
    <?php
      endif;
else:

    ?>



<h2><?=$_lang_decode_titre?></h2>
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <div class="form-group">
                            
                                    <span class="input-group-addon" ><label for="ln_invoice"><?=$_lang_ln_invoice?> :</label> 
                                    <input id="ln_invoice" name="ln_invoice" type="text" required="" placeholder = "ln...."  class="form-control input-md" /></span></div><div class="form-group">
                                    <span class="input-group-addon"><button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i></button></span>
                                </div>
                            
                        
                    </fieldset>
                </form>
            </div>
<?php endif; ?>