<html lang="fr">
<head>
    <meta charset="utf-8">
    <title> Lightning payment </title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/csst.css" />
    <link rel="stylesheet" href="css/my.css" />
    <link rel="stylesheet" href="css/fontawesome-all.css" />
    <link rel="stylesheet" href="css/modal.css" />
    <link rel="icon" href="eclair.ico" />
     <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>   <script>
    var clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>

</head>
<body>


<?= $header  ?>
<div class="jumbotron">
    <?= $menu ?>
            <div class="col-md-10 center-block" style="position: inherit;">

    <?php if ($langChanged): ?><div class="alert alert-success"><?= $_lang_changed ?></div>' <?php endif; 
     if(!$lnd_on): ?>
        <div class="alert alert-danger"><?= $_lang_lnd_off?></div>
    <?php elseif(!$connected): ?>

     <div class="alert alert-danger"><?= $_lang_not_connected ?></div>
           <?php if(!$unlock):?>
        <form method="post"> <div class="form-group">
            <label for="pwd">Password:</label>
             <input type="password" class="form-control" name="pwd"><input type="submit" class="btn btn-success" value="<?= $_lang_unlock ?>" /></div></form>
             <?php if(isset($_POST['pwd'])): ?><div class="alert alert-danger"><?=$connect_response['error']?></div> <?php endif ;endif;?>
    <?php 
    else:
        if(isset($_POST['pwd'])):?>
        <div class="alert alert-success"><?= $_lang_connected ?></div>
    <?php endif ;?>
    <?= $content ?><?php endif; ?>
        </div>
</div>
 <?= $footer ?>
</body>
</html>