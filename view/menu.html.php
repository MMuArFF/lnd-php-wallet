<?php
$active_balance = ''; $active_send = ''; $active_receive = ''; $active_node = ''; $active_channels = ''; $active_list = ''; $active_peer = ""; $active_list =''; $active_transactions = "";$active_listout = '';$active_decode = '';$active_sign = '';$active_verify = '';$active_logs='';

if(isset($_GET['peer'])){$active_peer = 'class="active"';}
if(isset($_GET['balance'])){$active_balance = 'class="active"';}
if(isset($_GET['nodeinfo'])){$active_node = 'class="active"';}
if(isset($_GET['channels'])){$active_channels = 'class="active"';}
if(isset($_GET['send'])){$active_send = 'class="active"';}
if(isset($_GET['list'])){$active_list = 'class="active"';}
if(isset($_GET['listout'])){$active_listout = 'class="active"';}
if(isset($_GET['transactions'])){$active_transactions = 'class="active"';}

if(isset($_GET['decode'])){$active_decode = 'class="active"';}
if(isset($_GET['sign'])){$active_sign = 'class="active"';}
if(isset($_GET['verify'])){$active_verify = 'class="active"';}
if(isset($_GET['logs'])){$active_logs = 'class="active"';}
if(empty($_GET) OR isset($_GET['receive'])){$active_receive = 'class="active"';}
?><div class="row">
            <div class="col-md-2">
                <div id="sidebar" class="well sidebar-nav">
                     <h5><i class="fab fa-bitcoin"></i>
                        <small><b>Onchain</b></small>
                    </h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li <?=$active_balance?>><a href="?balance"><?=ucfirst($_lang_balance)?></a></li>
                        <li <?=$active_transactions?>><a href="?transactions">Transactions</a></li>
                     </ul>
                    <h5><i class="fas fa-bolt"></i>
                        <small><b><?=$_lang_node_info?></b></small>
                    </h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li <?=$active_node?>><a href="?nodeinfo">Info</a></li>
                        <li <?=$active_channels?>><a href="?channels"><?=ucfirst($_lang_channels)?></a></li>
                        <li <?=$active_peer?>><a href="?peers"><?= ucfirst($_lang_peers) ?></a></li>
                    </ul>
                    <h5><i class="fas fa-bolt"></i>
                        <small><b>Lightning</b></small>
                    </h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li <?=$active_send?>><a href="?send"><?=ucfirst($_lang_send_wallet)?></a></li>
                        <li <?=$active_receive?>><a href="?receive"><?=ucfirst($_lang_receive_wallet)?></a></li>
                    </ul>
                    <h5><i class="fas fa-bolt"></i>
                        <small><b>Misc</b></small>
                    <ul class="nav nav-pills nav-stacked">
                        <li <?=$active_decode?>><a href="?decode"><?=ucfirst($_lang_decode)?></a></li>
                        <li <?=$active_logs?>><a href="?logs">Logs</a></li>
                         <li <?=$active_sign?>><a href="?send"><?=ucfirst($_lang_sign)?></a></li>
                        <li <?=$active_verify?>><a href="?receive"><?=ucfirst($_lang_verify)?></a></li>
                    </ul>
                    
                </div>
            </div>