<div style="float: left; width: 33%;" class=" alert alert-success">
<h5>In : <?=$balance['in']?> SAT</h5></div>
<div style="float: left; width: 33%;" class="alert alert-danger">
<h5>Out : <?=$balance['out']?> SAT</h5></div>
<div style="float: left; width: 33%;" class="alert alert-info">
<h5> Total : <?=$balance['total']?> SAT</h5></div>

<h2><?=$_lang_paiement_list?></h2>
<table class="table table-responsive"><thead><th>id</th><th>Transaction id</th><th><?=$_lang_amt?></th><th>date</th></thead><tbody>
        <?php  
        foreach ($transactions as $i => $rrow): 
            $rrow = array_reverse($rrow);
            foreach ($rrow as $ii => $row) 
            {
                $tx_id = $row['tx_hash'];
                $amt = $row['amount'];
                $time = date("m/d/Y H:i", $row['time_stamp']);
                $class_td = "success";
                $to = "up";
                if ($amt < 0) {
                    $class_td ="danger";
                    $to = "down";
                }
            ?>
        <tr class="<?= $class_td ?>">
            <td scope="row" ><i class="fas fa-arrow-<?= $to ?>"></i><?= $ii ?></td>
            <td width="500" ><div style="width: 500px;  overflow: auto"><a href="https://live.blockcypher.com/btc-testnet/tx/<?= $tx_id ?>"><?= $tx_id ?></a></div></td>
            <td><?= abs($amt) ?></td>
            <td><?= $time ?></td>
        </tr>
        <?php
            }
    endforeach;
        ?>
    </table>
</body>
</html>