
<div class="row">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="page-scroll navbar-brand" href="#intro"><?= $_lang_intro ?></a>
        </div>
        
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                     <?php if ($lang === "FR"): ?>
           <a href="?<?=$page?>&EN"><i class="fa fa-globe" aria-hidden="true"></i>EN</a>

    <?php elseif($lang === "EN"): ?>
       <a href="?<?=$page?>&FR"><i class="fa fa-globe " aria-hidden="true"></i>FR</a>

    <?php endif; ?>
                    </li>
                <li>
                    <a class="page-scroll" href="https://github.com/bcongdon/awesome-lightning-network" target="_blank" >Ressources</a>
                </li>
                
            </ul>
        </div>
        
    </div>
    
</nav>
</div>