<?php
function Action(LightningClient $client): string
{
	$array = array(
	  	'wbal' => $client->getWalletBalance(),
	    'cbal' => $client->getChannelBalance(),
	    'add' => $client->newadd(),
	    'qr' => generateqr($client->newadd()['address']) 
	);
	if(isset($_POST['addr']) && isset($_POST['amt']))
	{
	  $array['sendtx'] = $client->sendonchain($_POST['addr'],$_POST['amt']);
	}
	return template("../view/balance.html.php",$array);
}


 