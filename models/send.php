<?php
function Action($client): string
{
    $array = array();
    if(isset($_POST['ln_invoice']))
    {
        $array['invoice'] = $client->payinvoice(htmlentities($_POST['ln_invoice']));
        $array['res'] = $client->sendpayment(htmlentities($_POST['ln_invoice']));

    }
    $array['list'] = $client->getPayments();
    foreach ($array['list'] as $i => $rrow)
  			{
            	$rrow = array_reverse($rrow);
            }
    $array['page'] = paginate_array($rrow,5);
    return template('../view/send.html.php',$array);
}