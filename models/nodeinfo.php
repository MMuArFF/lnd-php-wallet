<?php
function Action($client): string
{	
	$info = $client->getInfo();
	//print_array($info);
	$uri = $info['uris'][0];
	$qr = generateqr($uri);
	return template("../view/node.html.php", [
	    'info' => $info,
	    'qr' =>$qr
	]);
}
?>
