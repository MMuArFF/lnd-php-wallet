<?php
function Action(LightningClient $client): string
{
    return template("../view/transactions.html.php", [
        'transactions' => $client->getTransactions(),
        'balance' => balance($client->getTransactions())
    ]);
}

?>