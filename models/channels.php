<?php
function Action($client): string
{
	$array = array(
		'chans' => $client->getAllchannels(),
		'channels' => getActiveandInactiveChannels($client->getAllchannels()),
    	'peers' =>$client->getPeers(), 
		'pending_channels' => $client->getpendigChannels());
	$ii = 0;
	foreach ($array['channels'] as $key => $chans) {
		foreach ($chans as $key => $id_chan) {
		$array['nodes_info_chans'][$id_chan] = $client->getNodeInfos($array['chans']['channels'][$id_chan]['remote_pubkey']);
		$ii++;
		}
	}
	$ii=0;
	foreach ($array['pending_channels'] as $key => $chans_pending) {
		if(!empty($chans_pending)):
		foreach ($chans_pending as $key => $i) {
			$array['nodes_info_chans_pending'][$ii] = $client->getNodeInfos($i['channel']['remote_node_pub']);
		$ii++;
		}
		endif;
	}
	if(isset($_POST['chan_node_addr'])&& isset($_POST['loc_amt']) && isset($_POST['rmt_amt'])):
	  $array['openchannel'] = $client->openchannel($_POST['chan_node_addr'],$_POST['loc_amt'],$_POST['rmt_amt']);
	endif;
	if(isset($_GET['id'])){
		
		$node_id  = $array['chans']['channels'][$_GET['id']]['remote_pubkey'];
		$array['node_info'] = $client->getNodeInfos($node_id);
	}
	if(isset($_POST['tx_id'])):
		if(isset($_POST['active'])):
  		$force = false;
  		else:
  		$force = true;
  		endif;

  		$array['closechannel'] =  $client->closechannel($_POST['tx_id'],$force);
	endif;
		return template("../view/channels.html.php", $array);
}
