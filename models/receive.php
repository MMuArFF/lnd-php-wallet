<?php 
function Action($client): string
{
    $array = array();
    if(isset($_POST['amt']) && is_numeric($_POST['amt']) &&  ctype_digit($_POST['amt']) && $_POST['amt'] > 0 )
    {
        isset($_POST['memo'])? $array['request'] = $client->addinvoice($_POST['amt'],$_POST['memo'] ): $array['request'] = $client->addinvoice($_POST['amt']);
        
        $array['qr'] = generateqr( $array['request']['payment_request']);

    }
    if(!empty($_POST['request']))
    {
        $array['request_dec'] = lookupinvoice($_POST['request']);
    }
    if(isset($_POST['payment'])):
        $pay_req = unserialize(base64_decode($_POST['payment']));
        $array['details'] = $client->payinvoice($pay_req['payment_request']);
        $array['qr'] = generateqr($pay_req['payment_request']);
        $array['node_info'] = $client->getNodeInfos($array['details']['destination']); 

    endif;
    $array['list'] =  $client->getInvoices();
    //print_array($array);
    foreach ($array['list']['invoices'] as $i => $rrow)
            {
                $rrow[$i] = array_reverse($rrow);
            }
    if(!empty($rrow)){
    $array['page'] = paginate_array($rrow,5);
    }
    else{$array['page']="";}
    return template("../view/receive.html.php",$array);
}
