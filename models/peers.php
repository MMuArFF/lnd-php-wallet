<?php
function Action($client): string
{
	$array = array('peers' => $client->getPeers());
	foreach ($array['peers'] as $key => $peers) {
		foreach ($peers as $key => $peer) {
			$peerInfo = $client->getNodeInfos($peer['pub_key']);
			$array['peers']['peers'][$key]['node_alias'] =$peerInfo['node']['alias'];
			$array['peers']['peers'][$key]['color'] =$peerInfo['node']['color'];
		}
		
	}
	if(isset($_POST['node_addr']))
	{
		
	  		$array['add_peer']= $client->connectPeers($_POST['node_addr']);
	}
	return template("../view/peers.html.php",$array);
}