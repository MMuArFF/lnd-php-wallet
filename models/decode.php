<?php
function Action($client): string
{
    $array = array();
    if(isset($_POST['ln_invoice']))
    {
        $array['invoice'] = $client->payinvoice(htmlentities($_POST['ln_invoice']));
        $temps_crea = $array['invoice']['timestamp'];
        $expiry = $array['invoice']['expiry'];
        $array['invoice']['diff_time'] = time() - ($temps_crea + $expiry);	
     	$array['node_info'] = $client->getNodeInfos($array['invoice']['destination']);   
    }
    return template('../view/decode.html.php',$array);
}